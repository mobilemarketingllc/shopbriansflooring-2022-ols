import React from "react";

export default function Material({ handleFilterClick, productMaterial }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productMaterial = sortObject(productMaterial);

  return (
    <div class="facet-wrap facet-display">
      <strong>Material</strong>
      <div className="facetwp-facet">
        {Object.keys(productMaterial).map((material, i) => {
          if (material && productMaterial[material] > 0) {
            return (
              <div>
                <span
                  id={`material-${i}`}
                  key={i}
                  data-value={`${material.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("material", e.target.dataset.value)
                  }>
                  {" "}
                  {material} {` (${productMaterial[material]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
