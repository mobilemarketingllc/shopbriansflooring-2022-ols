import React from "react";

export default function SampleAvailableFacet({
  handleFilterSample,
  productSamples,
  selectedSample,
}) {
  const sampleVal = selectedSample == "Yes" ? "" : "Yes";
  const sampleClass =
    selectedSample == "Yes" ? "sampleActive" : "sampleInactive";
  return (
    <div class="facet-wrap facet-display">
      <strong>Sample Available</strong>
      <div className="facetwp-facet">
        <div
          className={`smapleIcon ${sampleClass}`}
          id={`sample-available-filter`}
          data-value={sampleVal}
          onClick={(e) =>
            handleFilterSample("sample_available_facet", e.target.dataset.value)
          }>
          <span className="samplePoint"></span>
        </div>
        {`Yes (${productSamples["Yes"] ? productSamples["Yes"] : 0})`}
        {/* {Object.keys(productSamples).map((sample, i) => {
          if (sample && productSamples[sample] > 0) {
            return (
              <div>
                <span
                  id={`sample-available-filter-${i}`}
                  key={i}
                  data-value={`${sample.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick(
                      "sample_available_facet",
                      e.target.dataset.value
                    )
                  }>
                  {" "}
                  {sample} {` (${productSamples[sample]}) `}
                </span>
              </div>
            );
          }
        })} */}
      </div>
    </div>
  );
}
